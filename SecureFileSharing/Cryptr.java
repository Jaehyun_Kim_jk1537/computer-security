/*
 *               Cryptr
 *
 * Cryptr is a java encryption toolset
 * that can be used to encrypt/decrypt files
 * and keys locally, allowing for files to be
 * shared securely over the world wide web
 *
 * Cryptr provides the following functions:
 *	 1. Generating a secret key
 *   2. Encrypting a file with a secret key
 *   3. Decrypting a file with a secret key
 *   4. Encrypting a secret key with a public key
 *   5. Decrypting a secret key with a private key
 *
 */
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;



public class Cryptr {
	private static final String cipher_algorithm = "AES";
	private static final String cipher_algorithm_RSA = "RSA";
	private static final String CBC_mode_padding = cipher_algorithm + "/CBC/PKCS5Padding";
	private static final String ECB_mode_padding = cipher_algorithm_RSA + "/ECB/PKCS1Padding";

	/**
	 * Generates an 128-bit AES secret key and writes it to a file
	 *
	 * @param  secKeyFile    name of file to store secret key
	 */
	static void generateKey(String secKeyFile) throws Exception{
		
		//generate secret key
		KeyGenerator key_generate = KeyGenerator.getInstance(cipher_algorithm);
		key_generate.init(128);
		SecretKey skey = key_generate.generateKey();

		try (FileOutputStream out = new FileOutputStream(secKeyFile)) { // secKeyFile = secret.key
		    byte[] key_byte = skey.getEncoded();
		    out.write(key_byte);
		}
	}
	
	static private void encrypt_n_decrypt_process(Cipher cipher,String inFile,String outFile) throws javax.crypto.IllegalBlockSizeException, javax.crypto.BadPaddingException, java.io.IOException{
		
		try (FileInputStream in_key = new FileInputStream(inFile);
			 FileOutputStream out_key = new FileOutputStream(outFile)) {
			
			byte[] in_byte = new byte[1024];
		    int read = -1;
		    while ((read = in_key.read(in_byte)) != -1) {
		    	byte[] out_byte = cipher.update(in_byte, 0, read);
		        if ( out_byte != null ) 
		        	out_key.write(out_byte);
		    }
		    byte[] out_byte = cipher.doFinal();
		    if ( out_byte != null ) 
		    	out_key.write(out_byte);
		}
		
	}

	/**
	 * Extracts secret key from a file, generates an
	 * initialization vector, uses them to encrypt the original
	 * file, and writes an encrypted file containing the initialization
	 * vector followed by the encrypted file data
	 *
	 * @param  originalFile    name of file to encrypt
	 * @param  secKeyFile      name of file storing secret key
	 * @param  encryptedFile   name of file to write iv and encrypted file data
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	
	static void encryptFile(String originalFile, String secKeyFile, String encryptedFile) throws FileNotFoundException, Exception {
									// = foo.txt                             // foo.enc
		//get AES secret key to encrypt file
		byte[] key_byte = Files.readAllBytes(Paths.get(secKeyFile));
		SecretKeySpec secret_key = new SecretKeySpec(key_byte, cipher_algorithm);
		
		//create Initial vector
		byte[] iv = new byte[128/8];
		Random ran_byte = new Random();
		ran_byte.nextBytes(iv);
		IvParameterSpec ivspec = new IvParameterSpec(iv);
		
		try (FileOutputStream out = new FileOutputStream("ivFile.enc")) {
		    out.write(iv);
		}
		
		//encrypt file using secret key and iv
		//mode: CBC and padding
		Cipher cipher = Cipher.getInstance(CBC_mode_padding);
		cipher.init(Cipher.ENCRYPT_MODE, secret_key, ivspec);
		
		encrypt_n_decrypt_process(cipher, originalFile, encryptedFile);


	}
	


	/**
	 * Extracts the secret 		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
key from a file, extracts the initialization vector
	 * from the beginning of the encrypted file, uses both secret key and
	 * initialization vector to decrypt the encrypted file data, and writes it to
	 * an output file
	 *
	 * @param  encryptedFile    name of file storing iv and encrypted data
	 * @param  secKeyFile	    name of file storing secret key
	 * @param  outputFile       name of file to write decrypted data to
	 * @throws IOException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	static void decryptFile(String encryptedFile, String secKeyFile, String outputFile) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		//read key
		byte[] key_byte = Files.readAllBytes(Paths.get(secKeyFile));
		SecretKeySpec secret_key = new SecretKeySpec(key_byte, cipher_algorithm);
		
		//read Iv bytes from file 
		byte[] iv = Files.readAllBytes(Paths.get("ivFile.enc"));
		IvParameterSpec ivspec = new IvParameterSpec(iv);
		
		//decrypt file using secret key and iv
		//mode CBC with padding
		Cipher cipher = Cipher.getInstance(CBC_mode_padding);
		cipher.init(Cipher.DECRYPT_MODE, secret_key, ivspec);
		encrypt_n_decrypt_process(cipher, encryptedFile, outputFile);
		
	}


	/**
	 * Extracts secret key from a file, encrypts a secret key file using
     * a public Key (*.der) and writes the encrypted secret key to a file
	 *
	 * @param  secKeyFile    name of file holding secret key
	 * @param  pubKeyFile    name of public key file for encryption
	 * @param  encKeyFile    name of file to write encrypted secret key
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	static void encryptKey(String secKeyFile, String pubKeyFile, String encKeyFile) throws NoSuchAlgorithmException, FileNotFoundException, IOException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		//read public key which is created by using RSA
		//to use public key, need to use X509
		byte[] pub_bytes = Files.readAllBytes(Paths.get(pubKeyFile));
		X509EncodedKeySpec x509_ks = new X509EncodedKeySpec(pub_bytes);
		KeyFactory key_factory = KeyFactory.getInstance(cipher_algorithm_RSA); // get RSA instance to use key
		PublicKey pub = key_factory.generatePublic(x509_ks);
		
		//encrypt secret key using RSA public key
		Cipher cipher = Cipher.getInstance(ECB_mode_padding);
		cipher.init(Cipher.ENCRYPT_MODE, pub);
		try (FileInputStream in = new FileInputStream(secKeyFile);
		     FileOutputStream out = new FileOutputStream(encKeyFile)) {
			encrypt_n_decrypt_process(cipher, secKeyFile, encKeyFile);
		}

	}


	/**
	 * Decrypts an encrypted secret key file using a private Key (*.der)
	 * and writes the decrypted secret key to a file
	 *
	 * @param  encKeyFile       name of file storing encrypted secret key
	 * @param  privKeyFile      name of private key file for decryption
	 * @param  secKeyFile       name of file to write decrypted secret key
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	static void decryptKey(String encKeyFile, String privKeyFile, String secKeyFile) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		//read private key
		//use PKCS8 to use private key
		byte[] pvt_bytes = Files.readAllBytes(Paths.get(privKeyFile));
		PKCS8EncodedKeySpec pkcs8_ks = new PKCS8EncodedKeySpec(pvt_bytes);
		KeyFactory key_factory = KeyFactory.getInstance(cipher_algorithm_RSA);
		PrivateKey pvt = key_factory.generatePrivate(pkcs8_ks);
		
		//decrypt key
		Cipher cipher = Cipher.getInstance(ECB_mode_padding);
		cipher.init(Cipher.DECRYPT_MODE, pvt);
		try (FileInputStream in = new FileInputStream(encKeyFile);
		     FileOutputStream out = new FileOutputStream(secKeyFile)) {
			encrypt_n_decrypt_process(cipher, encKeyFile, secKeyFile);
		}

	}


	/**
	 * Main Program Runner
	 */
	public static void main(String[] args) throws Exception{

		String func;

		if(args.length < 1) {
			func = "";
		} else {
			func = args[0];
		}

		switch(func)
		{
			case "generatekey":
				if(args.length != 2) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr generatekey <key output file>");
					break;
				}
				System.out.println("Generating secret key and writing it to " + args[1]);
				generateKey(args[1]);
				break;
			case "encryptfile":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr encryptfile <file to encrypt> <secret key file> <encrypted output file>");
					break;
				}
				System.out.println("Encrypting " + args[1] + " with key " + args[2] + " to "  + args[3]);
				encryptFile(args[1], args[2], args[3]);
				break;
			case "decryptfile":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr decryptfile <file to decrypt> <secret key file> <decrypted output file>");
					break;
				}
				System.out.println("Decrypting " + args[1] + " with key " + args[2] + " to " + args[3]);
				decryptFile(args[1], args[2], args[3]);
				break;
			case "encryptkey":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr encryptkey <key to encrypt> <public key to encrypt with> <encrypted key file>");
					break;
				}
				System.out.println("Encrypting key file " + args[1] + " with public key file " + args[2] + " to " + args[3]);
				encryptKey(args[1], args[2], args[3]);
				break;
			case "decryptkey":
				if(args.length != 4) {
					System.out.println("Invalid Arguments.");
					System.out.println("Usage: Cryptr decryptkey <key to decrypt> <private key to decrypt with> <decrypted key file>");
					break;
				}
				System.out.println("Decrypting key file " + args[1] + " with private key file " + args[2] + " to " + args[3]);
				decryptKey(args[1], args[2], args[3]);
				break;
			default:
				System.out.println("Invalid Arguments.");
				System.out.println("Usage:");
				System.out.println("  Cryptr generatekey <key output file>");
				System.out.println("  Cryptr encryptfile <file to encrypt> <secret key file> <encrypted output file>");
				System.out.println("  Cryptr decryptfile <file to decrypt> <secret key file> <decrypted output file>");
				System.out.println("  Cryptr encryptkey <key to encrypt> <public key to encrypt with> <encrypted key file> ");
				System.out.println("  Cryptr decryptkey <key to decrypt> <private key to decrypt with> <decrypted key file>");
		}

		System.exit(0);

	}

}

