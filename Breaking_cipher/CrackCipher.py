import sys
import re
import string
import operator

tmp = dict()
tmp2 = dict()
cipherText = ""
knownText = ""
x=""

''' *******************************
Remove characters except alphabet
******************************* '''
parsing = " ABCDEFGHIJKLNMOPQRSTUVWXYZ"
def parsing_data(data):
    ret_parsing={}
    for key in data.keys():
        if key in parsing:
            result = data[key]
            ret_parsing[key] = result
    return ret_parsing


clean_text = ""
def clean_text(text):
	cleaned_text = re.sub('[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\'\\n1234567890"]','', text)
	return cleaned_text


''' **********************
         Open files
********************** '''
with open("ciphertext-sample.txt", 'r') as f:
	x = f.read()
	cipherText = clean_text(x)
	cipherText = cipherText.upper()
	#print(cipherText)
f.close

with open("knowntext-sample.txt", 'r') as f:
	temp1 = f.read()
	knownText = clean_text(temp1)
	knownText = knownText.upper()
	#print(knownText)
f.close


''' **********************
Freqency Test 1: Cipher text
********************** '''
def frequencyCounting_cipher(cipherText):
	for i in range(len(cipherText)):
		if cipherText[i] in tmp:
			tmp[cipherText[i]] += 1
		else:
			tmp[cipherText[i]] = 1

frequencyCounting_cipher(cipherText)
#sort_tmp = sorted(tmp.iteritems(), key=cnt, reverse=True)
cipher_ch = parsing_data(tmp)
#print(cipher_ch)
#print("\n")
#print(len(cipher_ch))

''' DEBUGGING
for i in sort_tmp:
   print(i[0] + ":" + str(i[1]))
'''

sort_cipher = sorted(cipher_ch.items(), key=operator.itemgetter(1), reverse=True)
#print(sort_cipher)
#print("\n\n\n\n")
#print(type(sort_cipher))


''' **********************
Freqency Test 2: known text
********************** '''
def frequencyCounting_known(knownText):
	for i in range(len(knownText)):
		if knownText[i] in tmp2:
			tmp2[knownText[i]] += 1
		else:
			tmp2[knownText[i]] = 1
frequencyCounting_known(knownText)
known_ch = parsing_data(tmp2)
#print("\n")
#print(len(known_ch))


sort_known = sorted(known_ch.items(), key=operator.itemgetter(1), reverse=True)

#for i in cipherText:
   #print(i)

def makeList(text):
	temp = []
	for index, tup in enumerate(text):
		ch, val = text[index]
		temp.append(ch)
	return temp

cipher_ch =[]
cipher_ch = makeList(sort_cipher)
#print(cipher_ch)


known_ch = []
known_ch = makeList(sort_known)
for i in range(len(known_ch)):
	if known_ch[i] == " ":
		known_ch[i] = "*"
#print(known_ch)

#print(type(cipher_ch))	

x= x.upper()
for j in range(len(known_ch)):
	for i in x:
		if i == cipher_ch[j] and i.isupper():
			#print(i,cipher_ch[j],known_ch[j])
			x = x.replace(i,known_ch[j].lower())
		elif i == cipher_ch[j] and i == " ":
			x = x.replace(i,known_ch[j].lower())
		else:
			continue
for i in x:
	if i == "*":
		x = x.replace(i, " ")
print(x)
