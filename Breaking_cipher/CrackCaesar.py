import string
import re
import sys

''' **************************
Set Command line arguments
************************** '''
#print(sys.argv[1])
#print(sys.argv[2])
cipherText = sys.argv[1]
dictionaryText = sys.argv[2]

''' ***********************************
Open dictionary to decrypt cipher text
*********************************** '''
dictionary_list = []
with open(dictionaryText, 'r') as f:
    data = f.readlines()
    for lines in data:
        getdic = lines.rstrip() #to remove newline character using rstrip()
        dictionary_list.append(getdic) #save as list to compare easily
f.close
#print(dictionary_list)

''' ************************
Find key from cipher text
************************ '''
line = []
with open(cipherText, 'r') as f:
    line = f.readline().split('.') #remove . to compare with dictionary
    #print(line)
f.close
#print(line)

sentence = line[0].lstrip() #get first paragraph and remove whitespace to find key 
#print(type(sentence))

#this is function to get key
def decrypt_func(encrypted_text, n): #encypted_text is cipher text and 'n' is used to decrypt key to find which is correct key.
    decode_str = "" 

    for i in encrypted_text:
        #if alphabet is capital letter
        if(ord(i) >= 65 and ord(i) <= 90):  
            if(ord(i) -n < 65):
                tmp=chr(ord(i)+(26-n))
            else:
                tmp=chr(ord(i)-n)
            decode_str += tmp
        
        elif(ord(i)>=97 and ord(i)<=122): # else if it is small letter
            if(ord(i) -n < 97):
                tmp=chr(ord(i)+(26-n))
            else:
                tmp=chr(ord(i)-n)
            decode_str += tmp
        
        else: #else it is neither capital nor small letter
            decode_str += i #just concatenate, such as '.', ',' '(', etc. 

    return decode_str

masterKey = 0
cnt = 0
cnt_list = []
ret = ""
temp = []
for i in range(1,26): #this is for searching key try to decrypt from 1 to 25 
    ret = decrypt_func(sentence,i) # get return sentance from decrypt_func 
    temp = ret.split() # set to list to compare with dictionary
    
    s = set(temp) # change to set to use intersection between dictionary list and decrypted text
    temp = [x for x in dictionary_list if x in s] #compare two lists
    cnt = len(temp) # calculate how decoded sentences match words in dictionaries. If the count is higher, then that is the key.
    #print(cnt)
    cnt_list.append(cnt) # save count as list to find which one is the most highest value

 #   print(temp)
    #print(decrypt_func(sentence,i) + " Key = " + str(i))
#print(cnt_list)

# find key index 
masterKey = cnt_list.index(max(cnt_list))+1
print(str(masterKey))

''' **************************************
Decrypt the whole file using found key
************************************** '''
data = ""
with open(cipherText, 'r') as f:
    data = f.read()
    #print(data)
f.close

result = decrypt_func(data, masterKey)
print(result)
